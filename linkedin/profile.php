<?php
define('DB_NAME','linkedin');
	define('DB_USER','root');
	define('DB_PASSWORD','');
	define('DB_HOST','localhost');

$link = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);

if ($link === false) {
  die("ERROR: Could not connect. " . mysqli_connect_error());
}

session_start();
if(!isset($_SESSION['logged_in'])) 
    header("Location: login.php");

?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel ="stylesheet" href="navbar.css"> 
<!------ Include the above in your HEAD tag ---------->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
function myFunction() {
   document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
   
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
</script>
 <script type="text/javascript">
        function myFunc(cid) {
           //alert(cid);


            $.ajax({
                data: 'cid=' + cid,
                url: 'ajaxUserId.php',
                method: 'POST', // or GET
                success: function(msg) {
                    alert(msg);
                    //document.getElementById("l"+cid).innerHTML = "Likes:"+msg;
                    
                }
            });
            //alert("l"+cid);
            //document.getElementById(cid).innerHTML = "Liked";
           // document.getElementById("l"+cid).innerHTML = msg;

            window.location = 'profile.php';
        }

    </script>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="../CSS/profile.css">


<!--navigation bar-->
<div class="dropdown">
    
    <button onclick="myFunction()" class="dropbtn">Dropdown</button> 

    <span id="myDropdown" class="dropdown-content">
      <input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
    <?php
                        $query="select * from users";
                        $result=mysqli_query($link,$query) or die("Failed");
                        while($row=mysqli_fetch_assoc($result))
                        {
                            echo '<a>'.$row['user_name'].'</a>';
                            echo "<button onclick='myFunc(" . $row['user_id'] . ")' type='submit' id=" . $row['user_id'] . ">Go</button>";
                        }
                        ?>
                  
    </span>
     
</div>

<!--div class="dropdown">
  <button onclick="myFunction()" class="dropbtn">Dropdown</button>
 
    <input type="text" placeholder="Search..">
</div-->
<div class="container">
    <div class="row">
        <div>
            <div class="well profile">
                <div class="col-sm-12">
                    <div class="col-xs-12 col-sm-8">
                        <?php
                        echo '<h2>'.$_SESSION['username'].'</h2>';
                        ?>
                        <p><strong>Country: </strong> 
                            <?php
                            $user=$_SESSION['username'];
                            $query="SELECT * FROM `users` where user_name='$user'";
                            $ifqueryran=mysqli_query($link,$query);
                            if($ifqueryran)
                            {
                                $querydata=mysqli_fetch_assoc($ifqueryran);
                                echo $querydata['user_country'];
                            }
                            else echo "Query failed";
                            ?> </p>
                        <p><strong>Email: </strong>                         <?php
                            $user=$_SESSION['username'];
                            $query="SELECT `user_id` FROM `users` where user_name='$user'";
                            $ifqueryran=mysqli_query($link,$query);
                            if($ifqueryran)
                            {
                                $querydata=mysqli_fetch_assoc($ifqueryran);
                                $userid=$querydata['user_id'];
                            }
                            
                            $query="SELECT * FROM `users` where user_name='$user'";
                            $ifqueryran=mysqli_query($link,$query);
                            if($ifqueryran)
                            {
                                $querydata=mysqli_fetch_assoc($ifqueryran);
                                echo $querydata['user_email'];
                            }
                            else echo "Query failed";
                            
                            ?> </p>
                            <p><strong>Organisation: </strong><?php
                            $query="SELECT `user_organization` FROM `users` where `user_name`='$user'";
                            $ifqueryran=mysqli_query($link,$query);
                            if($ifqueryran)
                            {
                                $querydata=mysqli_fetch_assoc($ifqueryran);
                                echo $querydata['user_organization'];
                            }
                            else echo "Query failed";
                            
                            ?> </p>
                        <p><strong>Skills: </strong>
                            <?php
                            $query="SELECT * FROM `skills` where `user_id_fk`='$userid'";
                            $ifqueryran=mysqli_query($link,$query);
                            if($ifqueryran)
                            {
                                while($querydata=mysqli_fetch_assoc($ifqueryran))
                                {
                                    echo '<span class="tags">'.$querydata['skills'].'</span> &nbsp';
                                }
                            }
                            else echo "Query failed";?>
                        </p>
                        <p><strong>Education: </strong>
                            <?php
                            $query="SELECT * FROM `education` where `user_id_fk`='$userid'";
                            $ifqueryran=mysqli_query($link,$query);
                            if($ifqueryran)
                            {
                                while($querydata=mysqli_fetch_assoc($ifqueryran))
                                {
                                    echo '<span class="tags">'.$querydata['education'].'</span> &nbsp';
                                }
                            }
                            else echo "Query failed";?>
                        </p>
                    </div>             
                    <div class="col-xs-12 col-sm-4 text-center">
                        <figure>
                            <img src="../images/dp.png" alt="" class="img-circle img-responsive">
                            <figcaption class="ratings">
                                <p>Ratings
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star-o"></span>
                                    </a> 
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>            
                <div class="col-xs-12 divider text-center">
                    <div class="col-xs-12 col-sm-4 emphasis">
                        <h2><strong> 20,7K </strong></h2>                    
                        <p><small>Followers</small></p>
                        <button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Follow </button>
                    </div>
                    <div class="col-xs-12 col-sm-4 emphasis">
                        <h2><strong>245</strong></h2>                    
                        <p><small>Following</small></p>
                        <button class="btn btn-info btn-block"><span class="fa fa-user"></span> View Profile </button>
                    </div>
                    <div class="col-xs-12 col-sm-4 emphasis">
                        <h2><strong>43</strong></h2>                    
                        <p><small>Snippets</small></p>
                        <div class="btn-group dropup btn-block">
                            <button type="button" class="btn btn-primary"><span class="fa fa-gear"></span> Options </button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu text-left" role="menu">
                                <li><a href="#"><span class="fa fa-envelope pull-right"></span> Send an email </a></li>
                                <li><a href="#"><span class="fa fa-list pull-right"></span> Add or remove from a list  </a></li>
                                <li class="divider"></li>
                                <li><a href="#"><span class="fa fa-warning pull-right"></span>Report this user for spam</a></li>
                                <li class="divider"></li>
                                <li><a href="#" class="btn disabled" role="button"> Unfollow </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>                 
        </div>
    </div>
</div>