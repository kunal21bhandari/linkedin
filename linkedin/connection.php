<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "linkedin";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
	 echo "<script type='text/javascript'>alert('Retry , connection error!');
           window.location='log.html';
		 </script>";
         exit();
}

?>