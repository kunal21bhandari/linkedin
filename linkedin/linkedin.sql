-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2019 at 01:55 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linkedin`
--

-- --------------------------------------------------------

--
-- Table structure for table `certification`
--

CREATE TABLE `certification` (
  `user_id_fk` int(11) NOT NULL,
  `certification` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certification`
--

INSERT INTO `certification` (`user_id_fk`, `certification`) VALUES
(1, 'nptel'),
(1, 'coursera'),
(2, 'udemy'),
(3, 'edx'),
(4, 'nptel'),
(4, 'udacity'),
(2, 'coursera'),
(3, 'coursera'),
(4, 'edx'),
(4, 'edx'),
(5, 'django');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `user_id_fk` int(11) NOT NULL,
  `education` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`user_id_fk`, `education`) VALUES
(1, 'bba from reva university bengaluru'),
(1, 'bba from reva university bengaluru'),
(2, 'btech in infromation science from NIT suratkal '),
(4, 'be from bms college bengaluru'),
(4, 'mtech in electronics and electricals from iit madras'),
(3, 'be in electronics and communication from bms college'),
(3, 'ms in electronics and communication from arlington university texas'),
(2, 'be in eee'),
(3, 'ms in information science from carngie melon '),
(3, 'ms in information science from carngie melon '),
(5, 'BTECH'),
(5, 'BTECH');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `user_id_fk` int(11) NOT NULL,
  `experience` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`user_id_fk`, `experience`) VALUES
(1, 'interned from 1 year at qualcom'),
(2, 'worked for 3 years in qualcom'),
(1, 'working presently in texas instruments'),
(2, 'working presently in samsung r&d'),
(3, 'interned for 6 months in qualcom'),
(3, 'working presently at cisco'),
(4, 'presently working in paypal'),
(4, 'summer internship at cisco for 2 months'),
(5, 'citrix');

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `user_id_fk` int(11) NOT NULL,
  `follow_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`user_id_fk`, `follow_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 1),
(5, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `post_id` int(11) NOT NULL,
  `user_id_fk` int(11) NOT NULL,
  `post` varchar(300) NOT NULL,
  `post_likes` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`post_id`, `user_id_fk`, `post`, `post_likes`) VALUES
(1, 1, 'good  morning', 23),
(2, 1, 'this link is very useful for html beginners\r\nhttps://www.w3schools.com/html/default.asp', 2),
(3, 1, 'linkedin rvce winners are ....we will announce after the break', 12),
(4, 1, 'hello this is rvce', 0),
(5, 2, 'cnc  cewgcwecbih ewichewicbn webc ciwec', 21),
(6, 4, 'cewcc biwqfioce', 500),
(7, 3, 'i am famous', 999),
(8, 2, 'me to hu pagal ...tanna taaana', 0),
(9, 5, 'i am feeling good..:)', 0),
(10, 1, 'i am happy ..wdjid', 0);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `user_id_fk` int(11) NOT NULL,
  `skills` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`user_id_fk`, `skills`) VALUES
(1, 'good in c++,python,java and web technologies'),
(2, 'good in c++,html,css,javascript'),
(4, 'good in r language'),
(4, 'good in html,css,nodejs'),
(3, 'python'),
(3, 'java'),
(3, 'java'),
(1, 'cricket '),
(1, 'cricket '),
(5, 'cricket ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `user_email` varchar(30) NOT NULL,
  `user_password` varchar(25) NOT NULL,
  `user_dob` date NOT NULL,
  `user_gender` varchar(6) NOT NULL,
  `user_country` varchar(25) NOT NULL,
  `user_contact` varchar(10) NOT NULL,
  `user_position` varchar(20) NOT NULL,
  `user_organization` varchar(50) NOT NULL,
  `link` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_password`, `user_dob`, `user_gender`, `user_country`, `user_contact`, `user_position`, `user_organization`, `link`) VALUES
(1, 'kunal', 'kunalbhandari15@gmail.com', 'lettel4562', '1998-07-21', 'male', 'india', '8951721798', 'student', 'rv college of engineering', 'NULL'),
(2, 'nagashreyas', 'nagashreyas1999@gmail.com', 'lettel1234', '1999-01-23', 'male', 'USA', '9591023023', 'employee', 'google', 'NULL'),
(3, 'pranav', 'pranavb@gmail.com', 'iam69pranav', '1998-07-21', 'male', 'china', '9845114578', 'entrepreneur', 'cocomo solutions', NULL),
(4, 'nidhi', 'nidhi@gmail.com', 'iamraunak69', '1998-02-12', 'female', 'russia', '9002323356', 'technical head', 'microsoft', NULL),
(5, 'pranay', 'pranay@gmail.com', 'er1234er', '1996-02-29', 'male', 'india', '8547123690', 'employee', 'citrix', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certification`
--
ALTER TABLE `certification`
  ADD KEY `user_id` (`user_id_fk`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD KEY `userid_fk` (`user_id_fk`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD KEY `userid` (`user_id_fk`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD UNIQUE KEY `user_id_fk` (`user_id_fk`,`follow_id`),
  ADD KEY `uid` (`user_id_fk`),
  ADD KEY `u_id` (`follow_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `user_id_fk` (`user_id_fk`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD KEY `user` (`user_id_fk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `certification`
--
ALTER TABLE `certification`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `education`
--
ALTER TABLE `education`
  ADD CONSTRAINT `userid_fk` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `experience`
--
ALTER TABLE `experience`
  ADD CONSTRAINT `userid` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `follow`
--
ALTER TABLE `follow`
  ADD CONSTRAINT `u_id` FOREIGN KEY (`follow_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `uid` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `user` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
